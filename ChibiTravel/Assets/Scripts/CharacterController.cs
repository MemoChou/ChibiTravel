﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour {

	public bool dead = false;
	public GameObject obstacleToCheck = null;

	private float time;

	public float speed = 3f;
	public float speedDurationTime = 5f; // time to increase speed
	public float speedIncreasePct = 0.1f; // speed percentage to increase comparing to the current speed
	public float maxSpeed = 6f;

	// Use this for initialization
	void Start () {
		time = Time.realtimeSinceStartup;
        this.gameObject.GetComponent<Animator>().SetInteger("status", 1);
    }
	
	// Update is called once per frame
	void Update () {
		if (speed < maxSpeed
			&& (Time.realtimeSinceStartup - time >= speedDurationTime)) {
			speed += speed * speedIncreasePct;
			time = Time.realtimeSinceStartup;
		}
	}

	void FixedUpdate () 
	{
		bool isChangedCharacter = Input.GetButton("Fire1");

		if (isChangedCharacter)
		{
			
		}

		if (!dead) {
			Vector2 newVelocity = GetComponent<Rigidbody2D> ().velocity;
			newVelocity.x = speed;
			GetComponent<Rigidbody2D> ().velocity = newVelocity;

		} else {
			//display button start again

		}

		if (obstacleToCheck) {
//			GeneratorScript script = GetComponent<GeneratorScript> ();

			//find a closest obstacles
//			GameObject closestObj = null;
//			foreach (var obj in script.objects) {
//				if (!closestObj) {
//					closestObj = obj;
//				} else {
//					if (closestObj.transform.position.x > obj.transform.position.x) {
//						closestObj = obj;
//					}
//				}
//			}

//			GameObject closestObj = script.objects [0];
			//check if character come over obstacles
//			if (closestObj && closestObj.transform.position.x < this.gameObject.transform.position.x) {
			if (obstacleToCheck.transform.position.x < this.gameObject.transform.position.x) {
				obstacleToCheck = null;
				//this.gameObject.GetComponent<Animator> ().SetBool ("action", false);
				//this.gameObject.GetComponent<Animator> ().SetBool ("fail", true);
                this.gameObject.GetComponent<Animator>().SetInteger("status", 1);
            }
		}
	}
		
	void OnTriggerEnter2D(Collider2D collider)
	{
		int obstacleType = 0;
		if (collider.gameObject.CompareTag ("river"))
			obstacleType = 1;
		else if (collider.gameObject.CompareTag ("rock"))
			obstacleType = 2;
		else if (collider.gameObject.CompareTag ("samurai"))
			obstacleType = 3;
		
		OnTriggerCharacterActionByType(collider.gameObject, obstacleType);
	}

	void OnTriggerCharacterActionByType(GameObject obstacles, int obstacleType) {
		int characterType = this.gameObject.GetComponent<CharacterType> ().type;

		if (characterType != obstacleType) {
			dead = true;
            this.gameObject.GetComponent<Animator>().SetInteger("status", 0);
        } else {
			obstacleToCheck = obstacles;
			obstacleToCheck.GetComponent<Renderer>().enabled = obstacleType == 1;
			//this.gameObject.GetComponent<Animator> ().SetBool ("action", true);
			//this.gameObject.GetComponent<Animator> ().SetBool ("fail", false);
            this.gameObject.GetComponent<Animator>().SetInteger("status", 2);

            if (obstacleType == 1) {
				SoundManagerBehaviour.GetInstance ().StartMusic (Sounds.FLY);
			} else if (obstacleType == 2) {
				SoundManagerBehaviour.GetInstance ().StopMusic (Sounds.FLY);
				SoundManagerBehaviour.GetInstance ().StartMusic (Sounds.HIT);
			} else {
				SoundManagerBehaviour.GetInstance ().StopMusic (Sounds.FLY);
				SoundManagerBehaviour.GetInstance ().StartMusic (Sounds.SHOOT);
			}
		}
	}

	void OnGUI()
	{
		DisplayRestartButton();
	}

	void DisplayRestartButton()
	{
		if (dead)
		{
			Rect buttonRect = new Rect(Screen.width * 0.35f, Screen.height * 0.45f, Screen.width * 0.30f, Screen.height * 0.1f);
			if (GUI.Button(buttonRect, "Tap to restart!"))
			{
				Application.LoadLevel ("game");
			};
		}
	}
}
