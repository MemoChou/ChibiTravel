﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine; 

public class ChangeAnimator : MonoBehaviour {

	public RuntimeAnimatorController doraemonAnimator;
	public RuntimeAnimatorController chaienAnimator;
	public RuntimeAnimatorController nobitaAnimator;
	private Animator currentAnimator;


	void Start () {
		currentAnimator = this.gameObject.GetComponent<Animator> ();
		currentAnimator.runtimeAnimatorController = doraemonAnimator;
		this.gameObject.GetComponent<CharacterType> ().type = 1;
	}


	void Update () {
		//only can change character when not doing action
		CharacterController script = GetComponent<CharacterController> ();

		if (script.obstacleToCheck == null && Input.GetMouseButtonDown(0)) {
			if (currentAnimator.runtimeAnimatorController == doraemonAnimator) {
				currentAnimator.runtimeAnimatorController = chaienAnimator;
				this.gameObject.GetComponent<CharacterType>().type = 2;
			}  else if (currentAnimator.runtimeAnimatorController == chaienAnimator) {
				currentAnimator.runtimeAnimatorController = nobitaAnimator;
				this.gameObject.GetComponent<CharacterType>().type = 3;
			}  else if (currentAnimator.runtimeAnimatorController == nobitaAnimator) {
				currentAnimator.runtimeAnimatorController = doraemonAnimator;
				this.gameObject.GetComponent<CharacterType>().type = 1;
			}
		}

	}
}
