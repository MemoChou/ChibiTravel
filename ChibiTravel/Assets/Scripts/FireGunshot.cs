﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireGunshot : MonoBehaviour {

	public float gunshotForce = 750.0f;


    private void OnTriggerEnter2D(Collider2D target)
    {
        if (target.gameObject.tag == "FirePoint")
        {
            GetComponent<Rigidbody2D>().AddForce(transform.right * gunshotForce);
            
        }
    }
}
