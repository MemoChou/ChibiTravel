﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunBackGround : MonoBehaviour {

	//Initialize variables

	public int backgroundCount;
	public float backgroundWidth;

	private float time;

	public float speed;
	public float speedDurationTime = 3f; // time to increase speed
	public float speedIncreasePct = 0.1f; // speed percentage to increase comparing to the current speed
	public float maxSpeed=5f;

	// Use this for initialization
	void Start () {
		time = Time.realtimeSinceStartup;
	}

	// Update is called once per frame
	void Update () {
		//Move left
		transform.Translate (Vector3.left *  speed  * Time.deltaTime);

		Vector3 pos = transform.position;
		if (pos.x < -backgroundWidth) {
			pos.x += backgroundWidth * backgroundCount;
			transform.position = pos;
		}

		if (speed < maxSpeed
			&& (Time.realtimeSinceStartup - time >= speedDurationTime)) {
			speed += speed * speedIncreasePct;
			time = Time.realtimeSinceStartup;
		}
	}
}
