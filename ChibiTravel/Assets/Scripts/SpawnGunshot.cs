﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnGunshot : MonoBehaviour {

    private float trackTime = 0.0f;
    public float spawnTime = 1.0f;

    public GameObject gunshot;
    public Transform spawnPoint;

    private void Update()
    {
        trackTime += Time.deltaTime;
        if(trackTime >= spawnTime)
        {
            trackTime = 0;
            Instantiate(gunshot, spawnPoint.position, spawnPoint.rotation);
        }
    }
}
