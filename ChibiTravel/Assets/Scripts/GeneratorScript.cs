﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GeneratorScript : MonoBehaviour {

	public GameObject[] availableScenes;
	
	public List<GameObject> currentScenes;
	
	private float screenWidthInPoints;

	public GameObject[] availableObjects;    
	public List<GameObject> objects;
	
	public float objectsMinDistance = 5.0f;    
	public float objectsMaxDistance = 10.0f;
	
	public float objectsMinY = -1.4f;
	public float objectsMaxY = 1.4f;
	
	public float objectsMinRotation = -45.0f;
	public float objectsMaxRotation = 45.0f; 

	public int firstTimeTutorial = 0; 
	public int totalObjects = 0; 

	// Use this for initialization
	void Start () {
		float height = 2.0f * Camera.main.orthographicSize;
		screenWidthInPoints = height * Camera.main.aspect;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate () {
		
		GenerateSceneIfRequried();

		GenerateObjectsIfRequired();    
	}

	void AddScene(float farhtestRoomEndX)
	{
		int randomRoomIndex = Random.Range(0, availableScenes.Length);

		GameObject room = (GameObject)Instantiate(availableScenes[randomRoomIndex]);

		float roomWidth = room.transform.FindChild("floor").localScale.x;
		float roomCenter = farhtestRoomEndX + roomWidth * 0.5f;

		room.transform.position = new Vector3(roomCenter, 0, 0);

		currentScenes.Add(room);			
	} 

	void GenerateSceneIfRequried()
	{
		List<GameObject> scenesToRemove = new List<GameObject>();
	
		bool addScene = true;        
		float playerX = transform.position.x;
		float removeSceneX = playerX - screenWidthInPoints;        
		float addSceneX = playerX + screenWidthInPoints;
		
		//6
		float farhtestSceneEndX = 0;
		
		foreach(var scene in currentScenes)
		{
			//7
			float sceneWidth = scene.transform.FindChild("floor").localScale.x;
			float sceneStartX = scene.transform.position.x - (sceneWidth * 0.5f);    
			float sceneEndX = sceneStartX + sceneWidth;                            
			
			//8
			if (sceneStartX > addSceneX)
				addScene = false;
			
			//9
			if (sceneEndX < removeSceneX)
				scenesToRemove.Add(scene);
			
			//10
			farhtestSceneEndX = Mathf.Max(farhtestSceneEndX, sceneEndX);
		}
		
		//11
		foreach(var room in scenesToRemove)
		{
			currentScenes.Remove(room);
			Destroy(room);            
		}
		
		//12
		if (addScene)
			AddScene(farhtestSceneEndX);
	}

	void AddObject(float lastObjectX)
	{
		if (firstTimeTutorial < 3) {
			// Random Object
			int randomIndex = firstTimeTutorial;

			// Create Its Instance
			GameObject obj = (GameObject)Instantiate(availableObjects[randomIndex]);

			// Set Its X Positions
			float objectPositionX = lastObjectX + Random.Range(objectsMinDistance, objectsMaxDistance);

			float objectPositionY = -2.5f;

			if (randomIndex != 0) {
				objectPositionY = -1.3f;
			}
				
			obj.transform.position = new Vector3(objectPositionX,objectPositionY,0); 

			// Add Project
			objects.Add(obj);            

			++firstTimeTutorial;
		} else {
			// Random Object
			int randomIndex = Random.Range(0, availableObjects.Length);

			// Create Its Instance
			GameObject obj = (GameObject)Instantiate(availableObjects[randomIndex]);

			// Set Its X Positions
			float objectPositionX = lastObjectX + Random.Range(objectsMinDistance, objectsMaxDistance);

			float objectPositionY = -2.5f;
		
			if (randomIndex != 0) {
				objectPositionY = -1.3f;
			}

			obj.transform.position = new Vector3(objectPositionX,objectPositionY,0); 

			// Add Project
			objects.Add(obj);            
		}
	}

	void GenerateObjectsIfRequired()
	{
		// Set Variables
		float playerX = transform.position.x;        
		float removeObjectsX = playerX - screenWidthInPoints;
		float addObjectX = playerX + screenWidthInPoints ;
		float farthestObjectX = 0;

		// Make a list of objects to remove
		List<GameObject> objectsToRemove = new List<GameObject>();

		// Add any Object that need to remove
		foreach (var obj in objects)
		{
			// Position Object
			float objX = obj.transform.position.x;

			// Farthest Object = Old Farthest Object or Object X
			farthestObjectX = Mathf.Max(farthestObjectX, objX);

			// Add remove object if the Object pass the screenPoints
			if (objX < removeObjectsX)            
				objectsToRemove.Add(obj);
		}

		// Destroy Object
		foreach (var obj in objectsToRemove)
		{
			objects.Remove(obj);
			Destroy(obj);
			totalObjects--;
		}

		// Add Object
		if (farthestObjectX < addObjectX) { 
			if (totalObjects < 6) {
				AddObject (farthestObjectX + 3f);
				totalObjects++;
			}
		}
	}
}
