﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Sounds
{
	BACKGROUND = 0,
	HIT = 1,
	FLY = 2,
	SHOOT = 3,
	FOOT_STEP = 4
}

/*  EXAMPLE HOW TO USE:
*	SoundManagerBehaviour.GetInstance().StartMusic(Sounds.SHOOT);
*/
public class SoundManagerBehaviour : MonoBehaviour {

	private AudioSource[] soundList;

	static private SoundManagerBehaviour instance = null;

	private SoundManagerBehaviour(){
		instance = this;
	}

	public static SoundManagerBehaviour GetInstance() {
		return instance;
	}

	// Use this for initialization
	void Start () {
		soundList = GetComponents<AudioSource> ();	
	}

	public void StartMusic(Sounds sID){
		int id = (int)sID;

		if (id < soundList.Length) {
			soundList [id].Play ();
		}
	}

	public void StopMusic(Sounds sID){
		int id = (int)sID;

		if (id < soundList.Length) {
			soundList [id].Stop ();
		}
	}

	public void StopAll(){
		for (int i = 0; i < soundList.Length; i++) {
			soundList [i].Stop ();
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
